# ===================================
# File: algo2_cycle.py
# Author: Jordan Ischard
# Description: mso files generator for starving-free problem. (algo2 in cycle)
# ===================================


from verilib.core.veripy import  synchronized_system, mutex, deadlock, EF, AG
from verilib.core.philib import philo, weak_equity, insecure_states, strong_equity
from verilib.core.interact import input_string

def generate_philo_cycle( n_philo : int, filename : str , withorder : bool ):
    
    if n_philo < 3:
        raise Exception( f"Invalid argument: Can build synchronized system with three or more philo: got {n_philo}" )

    syncro = synchronized_system( "System" )

    # creates philosophers
    sys_philo = philo()
    for i in range( 1, n_philo+1 ):
        syncro.add_system( f"p{i}", sys_philo )

    # creates mutex
    sys_mutex = mutex()
    for i in range( 1, n_philo  ):
        syncro.add_system( f'm{i}_{i+1}', sys_mutex )
    syncro.add_system(f'm{n_philo}_{1}', sys_mutex )

    # All philospohers
    for curr in range( 1, n_philo+1 ):

        # Boolean used for handle a specific case
        first = False

        # Name's transitions
        trans_hungry_name = f"p{curr}_hungry"
        trans_eat_name = f'p{curr}_eat'
        trans_think_name = f'p{curr}_think'


        if(curr == 1):  
            first = True
            prev, next = n_philo, (curr + 1)
        elif curr == n_philo :
            prev, next = (curr - 1)%n_philo, 1
        else :
            prev, next = (curr - 1)%n_philo, (curr + 1)
        mutex_p = f'm{prev}_{curr}'
        mutex_n = f'm{curr}_{next}'


        if(withorder):
            if(first):
                syncro.add_trans( trans_hungry_name, f'p{curr}:hungry,{mutex_n}:P' )
                syncro.add_trans( trans_eat_name, f'p{curr}:eat,{mutex_n}:is_P,{mutex_p}:P' )
                syncro.add_trans( trans_think_name, f'p{curr}:think,{mutex_n}:V,{mutex_p}:V' )
            else:
                syncro.add_trans( trans_hungry_name, f'p{curr}:hungry,{mutex_p}:P' )
                syncro.add_trans( trans_eat_name, f'p{curr}:eat,{mutex_p}:is_P,{mutex_n}:P' )
                syncro.add_trans( trans_think_name, f'p{curr}:think,{mutex_n}:V,{mutex_p}:V' )
                
            
        else: 
            syncro.add_trans( trans_hungry_name, f'p{curr}:hungry,{mutex_n}:P' )
            syncro.add_trans( trans_hungry_name, f'p{curr}:hungry,{mutex_p}:P' )
            syncro.add_trans( trans_eat_name, f'p{curr}:eat,{mutex_n}:P' )
            syncro.add_trans( trans_eat_name, f'p{curr}:eat,{mutex_p}:P' )
            syncro.add_trans( trans_think_name, f'p{curr}:think,{mutex_n}:V,{mutex_p}:V' )


    # deadlocks
    syncro.tag( "DEAD", EF( deadlock() ) )
    
    for i_philo in range( 1, n_philo + 1 ):
        p = f'p{i_philo}'
        # weak equity
        syncro.tag( f"WEAK_EQUITY_P{i_philo}", AG( weak_equity( p ) ) )
        
        # strong equite
        syncro.tag( f"STRONG_EQUITY_P{i_philo}", AG( strong_equity( p ) ) )

    # insecure state
    syncro.tag( "INSECURE", EF( insecure_states( n_philo, is_cyclic = True ) ) )

    if(withorder):
        syncro.todot( "cycle_with_order.dot" )
    else : 
        syncro.todot( "cycle_without_order.dot" )

    syncro.write_file( filename )


if __name__ == "__main__":
    try:
        n_philo = int(input("> Input number of philo: "))
        filename = input_string("Input filename", default="algo2_cycle.mso")
        withorder = int(input("> Choose between with order [1] and without order [0]: "))
        print(withorder)
        generate_philo_cycle( n_philo, filename, withorder )
    except Exception as e:
        print( "[!] An error occured: " + str(e) )
