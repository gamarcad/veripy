# ====================================
# File: interact.py
# Author: Gael Marcadet
# Description: Light user interaction library
# ===================================

def build_message( message, default = None ):
        if default != None:
            return f"> {message}[{default}]: "
        else:
            return f"> {message}: "

def input_int( message : str, default : int = None, **attr ):
    ''' Asks to user to input an integer '''
    
    
    def is_valid_int( result ):
        min = attr.get( 'min', None )
        if min is not None and result < min:
            return False
        
        max = attr.get( 'max', None )
        if max is not None and max < result:
            return False
        
        return True
        
    res = None
    while True:
        try:
            raw_input = input( build_message( message, default ))
            if raw_input == '' and default is not None:
                return default
            else:
                int_input = int( raw_input )
                if is_valid_int( int_input ):
                    return int_input
                else:
                    print( f"> Illegal int: not allows in current range" )
                
        except ValueError :
            print( f"Illegal value: not an integer" )

def input_string( message : str, default : str = None ):
    return input( build_message( message, default ) ) or default

