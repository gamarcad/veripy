# ==============================================
# File: philib.py
# Author: Gael Marcadet
# Description: Philosophers systems.
# Contains all systems used in algorithms
# introduces by Dijkstra.
# ==============================================

from verilib.core.veripy import system


def philo() -> system:
    ''' Creates and returns a philosopher system. '''
    # philo
    sys_philo = system( "Philosophe", 3 )
    sys_philo.add_prop( "think", 0 )
    sys_philo.add_prop( "hungry", 1 )
    sys_philo.add_prop( "eat", 2 )

    sys_philo.add_trans( 0, 1, "hungry" )
    sys_philo.add_trans( 1, 2, "eat" )
    sys_philo.add_trans( 2, 0, "think" )
    sys_philo.add_trans( 0, 0, "is_think" )
    sys_philo.add_trans( 1, 1, "not_think" )
    sys_philo.add_trans( 2, 2, "not_think" )

    return sys_philo

def edge() -> system:
    ''' Creates and returns an edge system. '''
    # edge
    edge = system( "Edge", 3, init=1 )

    edge.add_prop( "left", 0 )
    edge.add_prop( "center", 1 )
    edge.add_prop( "right", 2 )

    edge.add_trans( 1, 0, "left" )
    edge.add_trans( 1, 2, "right" )
    for src in range( 3 ):
        edge.add_trans( src, 1, "center" )

    for v in range( 3 ):
        if v == 0:
            name = "ileft"
        elif v == 1:
            name = 'icenter'
        else:
            name = 'iright'
        edge.add_trans( v, v, name )
    
    return edge

def strong_equity( philo : str ):
    ''' Returns strong equity property. '''
    return f"{philo}.hungry -> AF( {philo}.eat )"

def weak_equity( philo : str ):
    ''' Returns weak equity property.'''
    return f"{philo}.hungry -> EF( {philo}.eat )"


def insecure_states( n_philo, is_cyclic : bool ):
    ''' Returns insecure states where two neighbors philosophers eat at the same time. '''
    secure_philo = []
    if is_cyclic:
        secure_philo.append( f"p1.eat && p{n_philo}.eat" )

    for ip in range( 1, n_philo ):
        secure_philo.append( f"p{ip}.eat && p{ip + 1}.eat" )

    return  " || ".join( secure_philo )